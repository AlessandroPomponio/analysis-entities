package aentities

type Fingerprint struct {
	HasError  bool
	Error     string
	PHash     string
	Histogram []float64
}
